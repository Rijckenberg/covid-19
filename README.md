# COVID-19

COVID-19 symptom diary template in ODF / OpenDocument / Libreoffice format that can be used by patient to communicate diary of symptoms to a doctor

There are 5 sheets in the COVID-19 symptom diary template.

It is only necessary for the patient to fill in the first 2 sheets.

This digital file is useful to facilitate contactless transmission of patient data to the doctor.

The second sheet in the template contains sensitive data, especially regarding ethnicity data.

But the **ethnicity data should be recorded and publicly shared among governments , as allowed by**

**GDPR article 9 sections 2.i. and 2.j where the SARS-CoV-2 virus and COVID-19 disease are the serious cross-border threats to health**

